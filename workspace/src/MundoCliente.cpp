
#include <fstream>
#include "MundoCliente.h"
#include "glut.h"
#include <unistd.h>
#include <fcntl.h>
#include <sys/mman.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <math.h>
//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

CMundo::CMundo()
{
	Init();
}

CMundo::~CMundo()
{
	munmap(org, sizeof(datosm)); //Desproyectamos
	
	//Tubería cliente-servidor *************************
//	close(tuberiacs);
//	int unl=unlink("/tmp/tuberiacs"); //borramos la tuberia
//	if(unl<0)
//		perror("Error en unlink");


//	close(tuberiateclas);
//	int unl2= unlink("tmp/teclasfifo"); //borramos la tuberia
//	if (unl2<0)
//	            perror("Error en unlink");


}

void CMundo::InitGL()
{
	//Habilitamos las luces, la renderizacion y el color de los materiales
	glEnable(GL_LIGHT0);
	glEnable(GL_LIGHTING);
	glEnable(GL_DEPTH_TEST);
	glEnable(GL_COLOR_MATERIAL);
	
	glMatrixMode(GL_PROJECTION);
	gluPerspective( 40.0, 800/600.0f, 0.1, 150);
}

void print(char *mensaje, int x, int y, float r, float g, float b)
{
	glDisable (GL_LIGHTING);

	glMatrixMode(GL_TEXTURE);
	glPushMatrix();
	glLoadIdentity();

	glMatrixMode(GL_PROJECTION);
	glPushMatrix();
	glLoadIdentity();
	gluOrtho2D(0, glutGet(GLUT_WINDOW_WIDTH), 0, glutGet(GLUT_WINDOW_HEIGHT) );

	glMatrixMode(GL_MODELVIEW);
	glPushMatrix();
	glLoadIdentity();

	glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
	glDisable(GL_DEPTH_TEST);
	glDisable(GL_BLEND);
	glColor3f(r,g,b);
	glRasterPos3f(x, glutGet(GLUT_WINDOW_HEIGHT)-18-y, 0);
	int len = strlen (mensaje );
	for (int i = 0; i < len; i++) 
		glutBitmapCharacter (GLUT_BITMAP_HELVETICA_18, mensaje[i] );
		
	glMatrixMode(GL_TEXTURE);
	glPopMatrix();

	glMatrixMode(GL_PROJECTION);
	glPopMatrix();

	glMatrixMode(GL_MODELVIEW);
	glPopMatrix();

	glEnable( GL_DEPTH_TEST );
}
void CMundo::OnDraw()
{
	//Borrado de la pantalla	
   	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	//Para definir el punto de vista
	glMatrixMode(GL_MODELVIEW);	
	glLoadIdentity();
	
	gluLookAt(0.0, 0, 17,  // posicion del ojo
		0.0, 0.0, 0.0,      // hacia que punto mira  (0,0,0) 
		0.0, 1.0, 0.0);      // definimos hacia arriba (eje Y)    

	/////////////////
	///////////
	//		AQUI EMPIEZA MI DIBUJO
	char cad[100];
	sprintf(cad,"Jugador1: %d",puntos1);
	print(cad,10,0,1,1,1);
	sprintf(cad,"Jugador2: %d",puntos2);
	print(cad,650,0,1,1,1);
	int i;
	for(i=0;i<paredes.size();i++)
		paredes[i].Dibuja();


	for(i=0;i<disparos1.size();i++)  // Multiplicar el numero de esferas
                disparos1[i].Dibuja();

        for(i=0;i<disparos2.size();i++)  // Multiplicar el numero de esferas
                disparos2[i].Dibuja();



	fondo_izq.Dibuja();
	fondo_dcho.Dibuja();
	jugador1.Dibuja();
	jugador2.Dibuja();
	
	for(i=0;i<esferas.size();i++)
		esferas[i].Dibuja();
	/////////////////
	///////////
	//		AQUI TERMINA MI DIBUJO
	////////////////////////////

	//Al final, cambiar el buffer
	glutSwapBuffers();
}

void CMundo::OnTimer(int value)
{
        int i, j;	
	jugador1.Mueve(0.025f);
	jugador2.Mueve(0.025f);
        for(i=0;i<esferas.size();i++)
		esferas[i].Mueve(0.025f);
        for(i=0;i<disparos1.size();i++)   //Dar movimiento a los disparos
		disparos1[i].Mueve(0.025f);
	for(i=0;i<disparos2.size();i++)
		disparos2[i].Mueve(0.025f);
        
	for(i=0;i<paredes.size();i++)
	{
		paredes[i].Rebota(jugador1);
		paredes[i].Rebota(jugador2);
		for(j=0;j<esferas.size();j++)
 	               paredes[i].Rebota(esferas[j]);
	}

        for(i=0;i<esferas.size();i++){     //Rebotes de las raquetas con todas las esferas 

	     jugador1.Rebota(esferas[i]);
	     jugador2.Rebota(esferas[i]);  
        }

        for(i=0;i<esferas.size();i++){
	     if(fondo_izq.Rebota(esferas[i]))  //Rebote de fondo izq con todas las esferas
	     {
		esferas[i].centro.x=0;
		esferas[i].centro.y=rand()/(float)RAND_MAX;
		esferas[i].velocidad.x=2+2*rand()/(float)RAND_MAX;
		esferas[i].velocidad.y=2+2*rand()/(float)RAND_MAX;
		puntos2++;
             }
        }
  
        for(i=0;i<esferas.size();i++){    //Rebote de todas las esferas con el fondo dcho

             if(fondo_dcho.Rebota(esfera[i]))
	     {
		esferas[i].centro.x=0;
		esferas[i].centro.y=rand()/(float)RAND_MAX;
		esferas[i].velocidad.x=-2-2*rand()/(float)RAND_MAX;
		esferas[i].velocidad.y=-2-2*rand()/(float)RAND_MAX;
		puntos1++;
	      }        
        }


        if ((jugador1.Rebota(esferas[0]))||(jugador2.Rebota(esferas[0])))   //Aumenta la variable nrebote cada vez que la pelota
             botar ++;       // rebota

        for (i=0;i<esferas.size();i++)
        {
             if (esferas.size()==1 && (botar>3)){    //Se genera una nueva pelota cada 4 rebotes
                  Esfera e;
                  e.radio=1;
                  e.centro.x=0;
                  e.centro.y=rand()/(float)RAND_MAX;
                  e.velocidad.x=2+2*rand()/(float)RAND_MAX;
                  e.velocidad.y=2+2*rand()/(float)RAND_MAX;
                  esferas.push_back(e);
                  botar=0;


             }
         }

	for(int j=0;j<disparos1.size();j++)  //impacto de disparos1 en jugador2
		{
		if(jugador2.Rebota(disparos1[j]))
		{
			if((jugador2.y2-jugador2.y1)>1.5){
			jugador2.y2=jugador2.y2-0.5;
			jugador2.y1=jugador2.y1+0.5;}
			disparos1.erase(disparos1.begin(),disparos1.begin()+disparos1.size());   // borrar disparo al impactar	
		        }
		
                }


	for(int j=0;j<disparos2.size();j++)  //impacto de disparos2 en jugador1
		{
		if(jugador1.Rebota(disparos2[j]))
		{
			if((jugador1.y2-jugador1.y1)>1.5){
			jugador1.y2=jugador1.y2-0.5;
			jugador1.y1=jugador1.y1+0.5;}
			disparos2.erase(disparos2.begin(),disparos2.begin()+disparos2.size());	
		        }
                
		}

















//NUEVO PRACTICA4



	if((puntos1==3)||(puntos2==3))
		exit(0);

	


	switch(datosmpunt->accion){
		case 1: OnKeyboardDown('w',0,0); break;
		case -1: OnKeyboardDown('s',0,0); break;
		case 0: break;
	}




	datosmpunt->esfera=esferas[0];//devuelve posicion de la primera esfera
	datosmpunt->raqueta1=jugador1; //devuelve posición de raqueta1
	datosmpunt->raqueta2=jugador2; //devuelve posición de raqueta1

	char cad[200];
	//read(fifo_servidor_cliente,cad,sizeof(cad));
	s_comunicacion.Receive(cad, sizeof(cad));
	sscanf(cad,"%f %f %f %f %f %f %f %f %f %f %d %d", 
		&esferas[0].centro.x,&esferas[0].centro.y, 
		&jugador1.x1,&jugador1.y1,&jugador1.x2,&jugador1.y2, 
		&jugador2.x1,&jugador2.y1,&jugador2.x2,&jugador2.y2, 
		&puntos1, &puntos2); 
	}

void CMundo::OnKeyboardDown(unsigned char key, int x, int y)
{
	char tecla[]="0";
	switch(key)
	{
//	case 'a':jugador1.velocidad.x=-1;break;
//	case 'd':jugador1.velocidad.x=1;break;
	case 's':sprintf(tecla,"s");break;
	case 'w':sprintf(tecla,"w"); break;
	case 'l':sprintf(tecla,"l");break;
	case 'o':sprintf(tecla,"o");break;
/*	case 'c':
		if(disparos1.size()==0){
		Esfera e;
		e.radio=0.2;
		e.centro.x=jugador1.x1+0.6;
		e.centro.y=jugador1.y1+(jugador1.y2-jugador1.y1)/2;
		e.velocidad.y=0;
		e.velocidad.x=3;
		e.rojo=255;
		e.verde=0;
		e.azul=0;
		disparos1.push_back(e);
		}
		break;
	case 'm':
		if(disparos2.size()==0){
		Esfera e;
		e.radio=0.2;
		e.centro.x=jugador2.x1-0.6;
		e.centro.y=jugador2.y1+(jugador2.y2-jugador2.y1)/2;
		e.velocidad.y=0;
		e.velocidad.x=-3;
		e.rojo=0;
		e.verde=255;
		e.azul=0;
		disparos2.push_back(e);
		}
		break;*/
}
	s_comunicacion.Send(tecla,sizeof(tecla));

}

void CMundo::Init()
{
	Plano p;
        //pared inferior
	p.x1=-7;p.y1=-5;
	p.x2=7;p.y2=-5;
	paredes.push_back(p);

        //superior
	p.x1=-7;p.y1=5;
	p.x2=7;p.y2=5;
	paredes.push_back(p);

	fondo_izq.r=0;
	fondo_izq.x1=-7;fondo_izq.y1=-5;
	fondo_izq.x2=-7;fondo_izq.y2=5;

	fondo_dcho.r=0;
	fondo_dcho.x1=7;fondo_dcho.y1=-5;
	fondo_dcho.x2=7;fondo_dcho.y2=5;

	//a la izq
	jugador1.g=0;
	jugador1.x1=-6;jugador1.y1=-1;
	jugador1.x2=-6;jugador1.y2=1;

	//a la dcha
	jugador2.g=0;
	jugador2.x1=6;jugador2.y1=-1;
	jugador2.x2=6;jugador2.y2=1;


        //esferas
        Esfera e;
        e.radio=0.5;
        e.centro.x=0;
        e.centro.y=rand()/(float)RAND_MAX;
        e.velocidad.x=2+2*rand()/(float)RAND_MAX;
        e.velocidad.y=2+2*rand()/(float)RAND_MAX;
        esferas.push_back(e);


	//Tuberia
	
//	mkfifo("/tmp/tuberiacs", 0777); //crear tuberia 
//	int tuberiacs = open("/tmp/tuberiacs", O_RDONLY);//se abre tuberia en modo lectura
//	if(tuberiacs<0)
//		perror("Error al abrir");
	
	

//	mkfifo("/tmp/tuberiateclas",0777);
  //      int tuberiateclas=open("/tmp/tuberiateclas", O_WRONLY);
    //    if(tuberiateclas<0)
      //          perror("Error al abrir");


char ip[]="127.0.0.1";
	char nombre[50];
	printf("Introduzca su nombre:\n");
	scanf("%s",nombre);
	s_comunicacion.Connect(ip,8000);

s_comunicacion.Send(nombre,sizeof(nombre));

	int fd=open("/tmp/DatosComp.txt",O_RDWR|O_CREAT|O_TRUNC,0777);
	if(fd<0)
		perror("error al abrir el fichero");
	write (fd,&datosm,sizeof(datosm));
	//Convierte a char 
	org=(char*)mmap(NULL,sizeof(datosm),PROT_WRITE|PROT_READ,MAP_SHARED,fd,0);



	close (fd);
	datosmpunt=(DatosMemCompartida*)org;
	datosmpunt->accion=0;
//	datosmpunt->accion2=0;

}

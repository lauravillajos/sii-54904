#if !defined(AFX_MUNDO_H__9510340A_3D75_485F_93DC_302A43B8039A__INCLUDED_)
#define AFX_MUNDO_H__9510340A_3D75_485F_93DC_302A43B8039A__INCLUDED_

#include <vector>
#include "Plano.h"
#include "DatosMemCompartida.h"
#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#include "Esfera.h"
//#include "Raqueta.h"
#include <pthread.h>

#include <stdio.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>

#include "Socket.h"
#include <signal.h>
#include <sys/mman.h>

class CMundo  
{
public:
	void Init();
	CMundo();
	virtual ~CMundo();	
	
	void InitGL();	
	void OnKeyboardDown(unsigned char key, int x, int y);
	void OnTimer(int value);
	void OnDraw();	
	//Funcion comandos para las teclas cliente-servidor
	void RecibeComandosJugador();
	//Esfera esfera;
	std::vector<Esfera> esferas;
	std::vector<Esfera> disparos1;
	std::vector<Esfera> disparos2;
	std::vector<Plano> paredes;
	Plano fondo_izq;
	Plano fondo_dcho;
	Raqueta jugador1;
	Raqueta jugador2;

	double time;
	char *org;
	int puntos1;
	int puntos2;
	int botar=0;
	//atributo tuberia
	int tuberia;
//	int tuberiacs;
//	int tuberiateclas;
	//Variables de DatosMemCompartida
//	DatosMemCompartida datosm;
//	DatosMemCompartida *datosmpunt;
	pthread_t thid1;
	struct sigaction act;	
	Socket s_conexion;
Socket s_comunicacion;
};

#endif 
